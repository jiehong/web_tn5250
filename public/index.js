function onKeyDown(event) {
    if (event.key === 'F4') {
        document.getElementById(event.target.id + "-doc").showModal();
    }
}

function onKeyUp(event) {
    if (event.target.validity.valid) {
        event.target.parentNode.classList.remove("invalid-field");
    }
    if (!event.target.validity.valid) {
        event.target.parentNode.classList.add("invalid-field");
    }
}

function onRootKeyDown(event) {
    if (event.key === 'F2') {
        clickSubmitButton();
    }
}

function clickNextButton() {
    console.log('next');
}

function clickPreviousButton() {
    console.log('previous');
}

function clickSubmitButton() {
    document.getElementById("submitted").showModal();
}

addEventListener("DOMContentLoaded", () => {
    // Add dialog triggers on fields
    const inputs = document.querySelectorAll('input, select, fieldset');
    inputs.forEach((i) => {
      i.addEventListener("keydown", onKeyDown);
      i.addEventListener("keyup", onKeyUp);
    });
});
